const { _, parseDuration, isSet } = require('rappopo-sob').Helper
const Gpsd = require('node-gpsd-client')

module.exports = ({ sob }) => {
  sob.config.delayOnStart = parseDuration(sob.config.delayOnStart)
  return {
    settings: {
      singleService: true,
      webApi: { readonly: true }
    },
    started () {
      this.clients = {}
      setTimeout(() => {
        _.forOwn(sob.config.connection || {}, (v, k) => {
          this._connect(k)
        })
      }, sob.config.delayOnStart)
    },
    stopped () {
      _.forOwn(this.clients, (v, k) => {
        // v.close()
        this.logger.info(`Instance '${k}' closed`)
      })
    },
    actions: {
      connect (ctx) {
        return this._connect(ctx.params.name)
      },
      getAllClients (ctx) {
        return this.clients
      },
      getClient (ctx) {
        return this.clients[ctx.params.name]
      }
    },
    methods: {
      _connect (name) {
        // if (_.isEmpty(name)) name = 'default'
        const config = _.get(sob.config, 'connection.' + name)
        if (!config) {
          this.logger.error(`Invalid connection: ${name}`)
          return
        }
        if (isSet(config.reconnectInterval)) config.reconnectInterval = parseDuration(config.reconnectInterval) / 1000
        if (isSet(config.reconnectThreshold)) config.reconnectThreshold = parseDuration(config.reconnectThreshold) / 1000
        const client = new Gpsd(_.omit(config, ['autoConnect']))
        this.clients[name] = client
        client.name = name
        client.on('connected', () => {
          this.broker.broadcast(`gpsd:connect:${name}`, client)
          this.logger.info(`'${name}' connected`)
        })
        client.on('error', err => {
          this.broker.broadcast(`gpsd:error:${name}`, err)
          this.logger.error(`'${name}' error: ${err.message}`)
        })
        client.on('disconnected', () => {
          this.broker.broadcast(`gpsd:disconnect:${name}`)
          this.logger.warn(`'${name}' disconnected`)
        })
        client.on('reconnecting', () => {
          this.broker.broadcast(`gpsd:reconnect:${name}`)
          this.logger.info(`'${name}' reconnecting`)
        })
        if (config.autoConnect) client.connect()
      }
    }
  }
}
